/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b1;

/**
 *
 * @author Usuari
 */
 

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
 
 
public class JugantExecutorSchedule {
 
 
public static void main(String[] args) throws InterruptedException, ExecutionException {

    // crea un pool únic
    ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
   
    
    // Crear objecte Runnable.
    Runnable myFill = new ExecutaFil();
    
    // Programa Fil, s'inicia immediatament 0 seg i després es va executant cada 5 segons
    executor.scheduleWithFixedDelay(myFill, 0, 5, TimeUnit.SECONDS);
    
    
    // Espera per acabar 50 segons
    executor.awaitTermination(50, TimeUnit.SECONDS);
    
        // shutdown .
        executor.shutdown();
    
    
     System.out.println("Completat");
    }



    

}