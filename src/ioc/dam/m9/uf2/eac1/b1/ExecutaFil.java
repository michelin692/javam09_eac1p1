/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b1;

import java.util.Random;

/**
 *
 * @author Usuari
 */

// Fil Runnable
 class ExecutaFil implements Runnable {
     
        String[] myArray = {"Defensa Defensa","Vinga Barça, vinga Barça!","Tots junts fem força!!"};
        
        Random miNumeroRandom = new Random();
        
        @Override
        public void run() {
            System.out.println("Frase del aficionats als 5 seg: " + myArray[miNumeroRandom.nextInt(myArray.length)]);
        }
        
        public String[] getMyArray() {
            return myArray;
        }

        public void setMyArray(String[] myArray) {
            this.myArray = myArray;
        }

        public Random getMiNumeroRandom() {
            return miNumeroRandom;
        }

        public void setMiNumeroRandom(Random miNumeroRandom) {
            this.miNumeroRandom = miNumeroRandom;
        }
        
}
 