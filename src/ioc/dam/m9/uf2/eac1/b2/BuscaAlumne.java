//package ioc.dam.m9.uf2.eac1.Ex2;
package ioc.dam.m9.uf2.eac1.b2;

import java.util.ArrayList;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author 
 */
public class BuscaAlumne extends RecursiveTask<Alumne> {

    private ArrayList<Alumne> llistat;
    private int primer;
    private int ultim;
    private int codi;
    private static boolean trobat = false;

    public BuscaAlumne(ArrayList<Alumne> llistat, int primer, int ultim, int codi) {
        this.llistat =  llistat;
        this.primer = primer;
        this.ultim = ultim;
        this.codi = codi;
    }
    
    //buscaAlumneSequencial
    private Alumne buscarAlumeSeq(){
        
        for (int x = primer; x < llistat.size(); x++) {
            if( llistat.get(x).codi ==  codi ){
                trobat = true;
            };
        }
        if( trobat ){
            return llistat.get(codi);
        }else{
            return null;
        }
    }
    
    //buscaAlumneRecursiu
    private Alumne buscarAlumeReq(){
       // return null;
       BuscaAlumne task1;
       BuscaAlumne task2;
       int mig = (primer+ultim)/2;
       task1 = new BuscaAlumne(llistat, primer, mig,codi);
       task1.fork();
       task2 = new BuscaAlumne(llistat, mig+1, ultim , codi);
       task2.fork();
       
      return (Alumne) task1.join();
       
    }

    @Override
    protected Alumne compute() {
         //Implementa
        if( ultim - primer <= 10 ){
            return buscarAlumeSeq();
        }else{
            return buscarAlumeReq();
        }
    }
    
}


