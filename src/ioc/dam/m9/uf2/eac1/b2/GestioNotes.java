//package ioc.dam.m9.uf2.eac1.Ex2;
package ioc.dam.m9.uf2.eac1.b2;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;

/**
 *
 * @author
 */
public class GestioNotes {

    public static void main(String[] args) {
            
        try{
        
            int codiAlumne;
            Scanner entrada = new Scanner(System.in);

            //Implementa
            System.out.println ("Introdueix el codi de l'alumne a buscar: ");
            codiAlumne = entrada.nextInt();
            //System.out.println ("la nota es " + codiAlumne);

            System.out.println ( "Inicio cálculo"); 
            ForkJoinPool pool = new ForkJoinPool();

            ArrayList<Alumne> alumnes = new CreaAlumnes().obtenirLlistat();

            BuscaAlumne tarea = new BuscaAlumne(alumnes, 0, alumnes.size(),codiAlumne);
           
            long time = System.currentTimeMillis (); 

            // llama la tarea y espera que se completen 
            Alumne result1 = pool.invoke(tarea); 
            // máximo 
           //Alumne result = tarea.join(); 
            System.out.println ( "Tiempo utilizado:" + (System.currentTimeMillis () - time)); 
            if(result1 instanceof Alumne){
                System.out.println ( "el codi de l'alumne es: " + result1.codi +" té una qualificació: " + result1.notaF );
            }else{
                System.out.println ( "No existe el alumno" );
            }
            
            
        
        }catch(Exception e){
             System.out.println ("Mensaje de eror " + e);
        }
    }

}
